# Definitions

## Definition-of-Done
This section defines what are the requirements of a done task. If the listed requirements are not met the task can't be moved to close and will remain open until all all the requirements are fulfilled.

**The requirements are:**

- Code has been developed in a separate feature/fix branch.
- Code meets all its requirements and produces correct output.
- Code doesn't produce any errors or warnings.
- Code has been tested by the developer.
- Code has been reviewed by following the process of merge request.
- Code has been successfully merged in to the main branch.
- Code is properly commented.
- Documentation is up to date.