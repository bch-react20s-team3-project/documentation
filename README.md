# Jobvious
We are Team 3 and this is the documentation repository for our app, Jobvious, a Tinder for jobs :)  
![Janne](https://gitlab.com/uploads/-/system/user/avatar/7707970/avatar.png?width=32)
![Marianna](https://gitlab.com/uploads/-/system/user/avatar/7707981/avatar.png?width=32)
![Niko](https://gitlab.com/uploads/-/system/user/avatar/7708022/avatar.png?width=32)
![Vesa](https://gitlab.com/uploads/-/system/user/avatar/5189360/avatar.png?width=32)

Below you'll find all key details of the project and the application itself.

## Project overview
As the client described it - “...the idea is, that when employee user opens the app, it will show different job opportunities. And user can select whether they are interested or not. And the same idea from employer user, but instead of job opportunities it will show possible applicants (employee users). If both sides click ‘yes’ on each other, it is a match, and they can contact through the app for more details!”

Our goal for the project is to build something that lives up to and exceeds the expectations of the client and what we as a team can be proud of at the end of the project. We target to achieve a fully-functioning high-quality MVP app that's build using latest web development technologies and methodologies (such as agile and CI/CD) and deployed live on the internet at the end of the project in May.

## Development framework overview
We are utilizing Scrum, please check our [Scrum markdown](./SCRUM.md) for details.

For detailed development related guidelines, please check our [Development markdown](./DEVELOPMENT.md).

## Visual design for the user interface
All of our visual designs, storyboards, and mock-ups can be found on [Figma](https://www.figma.com/file/RpryC2aAhHaLeooJ6mPKBV/Telia_Project_StroryBoard%26MockUp?node-id=100%3A2).
![Design](./img/design.png)

For details on accessibility, please refer to its [separate markdown](./ACCESSIBILITY.md).
  
## Technical architecture
The following diagrams are created using [Draw.io](https://app.diagrams.net/). You can edit these also directly in VSCode e.g. by installing extension `hediet.vscode-drawio`.
### High-level execution and application architectures
![Architecture](./img/architecture.svg)

Please refer to the [web-ui repository](https://gitlab.com/bch-react20s-team3-project/web-ui) for the user interface code and the [server repository](https://gitlab.com/bch-react20s-team3-project/server) for the backend/db-access code.  
You can refer to [environments markdown](./ENVIRONMENTS.md) for more details on the environment configurations.

### Integration and data architecture
![Data model](./img/database.svg)

The models and the APIs to access them are also documented in a separate [markdown](./MODELS.md) for more convenient diffs etc.
