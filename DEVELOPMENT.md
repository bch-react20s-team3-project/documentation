# Project development guidelines
## Branching strategy
- **master** for production
- **dev** for integrated staging (our main branch)
- **feat/fix** branches for individual features/fixing

Our production/main branches are protected from direct pushes and only accept changes thru the GitLab merge request process.

## Making changes
There are a few things you need to consider to be able to commit/push changes into any of the project repositories:
- We are using `commitlint` and `husky` for git commit hooks. Remember to install husky locally (by running `npx husky install`) and consider the conventional commit message conventions (https://commitlint.js.org/#/concepts-commit-conventions) before commiting changes.
- Our project repositories are configured to accept only branches that follow our branch naming convention `(master|dev|feat|fix)\/*`, that is e.g.
  - `feat/my-cool-new-feature`
  - `fix/some-silly-mistake`
  
## Setting up your workspace in VSCode
You should set up your [VSCode workspace](https://code.visualstudio.com/docs/editor/workspaces) by adding each of the group repositories (GitLab projects) into a single workspace. This way you'll be able to take advantage of the included launch/debug configurations. E.g. of a workspace configuration file:
```json
{
	"folders": [
		{
			"path": "documentation"
		},
		{
			"path": "server"
		},
		{
			"path": "web-ui"
		}
	]
}
```