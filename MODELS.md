# API and database models

In general, you should refer to our [Swagger documentation](https://bch-team3-server-staging.herokuapp.com/documentation/v1.0.0#) - models and APIs generated purely by Strapi plugins are only documented there.

Below you can find the master documentation for the models that have been customized as they have been implemented into our Strapi backend server. Please note that these models also will have Strapi generated properties (like ids) which are not mentioned here - refer to the Swagger docs for the exhaustive documentation.

## User related models

### User (extensions of the Strapi users-permissions plugin model)

- contacts: one-to-many relation (unidirectional) to Contact
- matches: dynamic relation (unidirectional) to PositionMatch (one-to-one) / TalentMatch (one-to-many)
- profile: dynamic relation (unidirectional) to EmployerProfile (one-to-one, reverse: EmployerProfile.owners) / TalentProfile (one-to-one, reverse: TalentProfile.owner)

## Profile related models

### EmployerProfile

- owners: one-to-many relation (unidirectional) to (employer) User(s) (reverse: User.profile)
- companyName: Text
- positions: one-to-many relation (bidirectional) to PositionProfile (reverse: PositionProfile.owner)
- img: Long text (base64 encoded data)
- draft: Boolean

### TalentProfile

- owner: one-to-one relation (unidirectional) to User (reverse: User.profile)
- shortDescription: Text
- location: Text
- workExperience: Number
- jobTypeInterest: Enumeration
  - full-time
  - part-time
  - freelance
- skills: one-to-many relation (unidirectional) to Skill
- img: Long text (base64 encoded data)
- description: Long text
- draft: Boolean

### PositionProfile

- name: Text
- description: Long text
- location: Text
- requiredExperience: Number
- requiredSkills: one-to-many relation (unidirectional) to Skill
- img: Long text (base64 encoded data)
- jobType: Enumeration
  - full-time
  - part-time
  - freelance
- owner: many-to-one relation (bidirectional) to EmployerProfile (reverse: EmployerProfile.positions)

### Skill

- skillName: Text

## Messaging related models

### Contact

- seen: Boolean
- withUser: one-to-one relation (unidirectional) to User
- status: Enumeration
  - liked
  - matched

### Message

- seen: Boolean
- message: Text
- from: Text
- to: Text

## Matching related models

### TalentMatch

- forPosition: one-to-one relation (unidirectional) to PositionProfile
- filtered: one-to-many relation (unidirectional) to TalentProfile
- liked: one-to-many relation (unidirectional) to TalentProfile
- matched: one-to-many relation (unidirectional) to TalentProfile
- ignored: one-to-many relation (unidirectional) to TalentProfile
- owner: one-to-one relation (unidirectional) to User

### PositionMatch

- filtered: one-to-many relation (unidirectional) to PositionProfile
- liked: one-to-many relation (unidirectional) to PositionProfile
- matched: one-to-many relation (unidirectional) to PositionProfile
- ignored: one-to-many relation (unidirectional) to PositionProfile
- owner: one-to-one relation (unidirectional) to User

## Application interface usability related models

### Phrase

- text: Long text
- label: Text
