# How we are applying Scrum
Our Definition-of-Done can be found in the [Definitions markdown](./DEFINITIONS.md)
## Sprints overview
Sprint # | Dates | Sprint goal | Achieved / planned velocity (points)
--- | --- | --- | ---
Sprint 0 | 24.3. - 26.3. | **Research** the app domain, **orient/align** our team for the project, and get started with **visual/technical design** | 16 / 18
Sprint 1 | 29.3. - 9.4. | Complete **visual/technical design**, **setup for development** and **CI/CD**, and implement **initial frontend DOM/component structure** | 41 / 39
Sprint 2 | 12.4. - 23.4. | Getting comfortable with Gatsby, Strapi, Socket.io and GraphQL, creating **initial implementation for both frontend and backend** | 50 / 49
Sprint 3 | 26.4. - 7.5. | **Integrate frontend and backend** implementations, implement **chat**, and **polish styles** | 58 / 69
Sprint 4 | 10.5. - 21.5. | **Registering new users**, **authentication and authorization**, and final **polishing** | 40 / 59

## Team schedule
- Dailies - 15min daily 09:15-09:30
- Sprint planning - ~1/2 day once every sprint - 24.3. + 29.4. + 12.4. + 26.4. + 10.5.
- Sprint review prep sessions - before each review session on Fri mornings
- Sprint reviews - 15min every 2nd Fri afternoon
- Sprint retros - ~1h on same day as the sprint review
- Code freezes on Wed EOD before each review

## Scrum mastering responsibility
- Sprint 0-1 - **Vesa**
- Sprint 2 - **Janne**
- Sprint 3 - **Marianna**
- Sprint 4 - **Niko**

## Checklist for the scrum master:
Here are some key pointers what the SM should be doing (and when they should be doing it is mentioned in the parenthesis)
- Schedule and facilitate the ceremonies (dailies, plannings, reviews, retros)
- Ensure everything that is being worked on has a corresponding issue card on the GitLab group board and that the card is up to date (daily)
  - E.g. check there are no closed issues or merged/closed merge requests that are not tracked to any milestone:
    - https://gitlab.com/bch-react20s-team3-project/documentation/-/issues?scope=all&utf8=%E2%9C%93&state=closed&milestone_title=None
    - https://gitlab.com/bch-react20s-team3-project/documentation/-/merge_requests?scope=all&utf8=%E2%9C%93&state=closed&milestone_title=None
- Track if the work planned to the sprint is proceeding as scheduled, e.g. check burndowns and check if there are any overdue work items (daily)
- Fill in for a missing PO e.g. with product backlog grooming (at least once per sprint, before next sprint planning)
