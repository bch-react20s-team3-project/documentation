# Kickoff notes
## Fixed dates
![Timetable](./img/timetable.jpg)

## Project - "Tinder for jobs"  
![Project description](./img/project_description.jpg)

## System functionalities
![Required features](./img/required_features.jpg)
- IMPORTANT: Matching algorithm?

![Optional features](./img/optional_features.jpg)

## Non-functionals / constraints
![Other must-have expectations](./img/constraints.jpg)
- IMPORTANT: Configurable theme?
- IMPORTANT: a11y Level AA is a must:  https://www.w3.org/WAI/WCAG21/quickref/ ?

![Appreciated features](./img/qualities.jpg)

## Data structure
![Data model](./img/data_structure.jpg)

## Stakeholders
- Client: Telia
  - Eero Suvanto - Scrum Master - eero.suvanto@teliacompany.com
  - Roman Tuomisto - Developer - roman.tuomisto@teliacompany.com
- Coaches: BCH
  - Margit
  - Hoang
