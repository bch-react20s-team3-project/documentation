For testing accessibility we can use Wave https://wave.webaim.org/extension/
Wave is an all-inclusive web accessibility evaluation tool designed to check web applications for general accessibility issues. You can download the tool as a Chrome extension and click on the icon while visiting a web page to receive visual feedback about the accessibility of your content.The reporting is 100% private and secure. 

It displays the Alerts and provides reference information about them, what they mean, how to fix it and gives an url to the WAI website where this accessibility detail is described.


Lighthouse https://developers.google.com/web/tools/lighthouse
Lighthouse is an open-source, automated tool for improving the quality of web pages. You can run it against any web page, public or requiring authentication. It has audits for performance, accessibility, progressive web apps, SEO and more.
You can run Lighthouse in Chrome DevTools, from the command line, or as a Node module. You give Lighthouse a URL to audit, it runs a series of audits against the page, and then it generates a report on how well the page did. From there, use the failing audits as indicators on how to improve the page. Each audit has a reference doc explaining why the audit is important, as well as how to fix it.

To use press Inspect -> Lighthouse tab in developers tools -> Tick the categories you need and device -> Generate report


To quickly check background image contrast with the text, Margit recommended this page fore me: https://www.brandwood.com/a11y/
I tested our background image from hifi mock-up and it had passed fully. 
 

The Check-list that uses Web Content Accessibility Guidelines (WCAG) can be used as a reference point: https://www.a11yproject.com/checklist/
