# Project environments
## Development
Development environment can be set-up either completely **locally** by running each component on your local machine. Remember to start them in order from the bottom-up:
- Start a local MongoDB instance
- Start `server` locally
- Start `web-ui` locally

Remember the local environment configurations:
- `server` example:
```
DATABASE_HOST=127.0.0.1
DATABASE_SRV=false
DATABASE_SSL=false

# User/pass do not have to be set, assuming your MongoDB instance accepts connections without authentication
#DATABASE_USERNAME=db_user
#DATABASE_PASSWORD=db_pass
```
- `web-ui` example:
```
STRAPI_URL=http://localhost:1337 # Assuming default Strapi configuration

# User/pass can be anything, as long as the user is added on Strapi
STRAPI_USER=test@test.com 
STRAPI_PASSWORD=test1234 
```

You can also use the **shared online** development instances in case you're e.g. only working on the front-end.
- MongoDB: `cluster0.73hdu.mongodb.net/jobvious_dev_db`
- Server: `bch-team3-server-staging.herokuapp.com`

## Staging
- Web-Ui: `jobvious-staging.netlify.app/`
- Server: `bch-team3-server-staging.herokuapp.com`
- MongoDB: `cluster0.73hdu.mongodb.net/jobvious_stage_db`

## Production
- Web-Ui: //TODO @Vesa `jobvious.netlify.app/`
- Server: `bch-team3-server-production.herokuapp.com`
- MongoDB: `cluster0.73hdu.mongodb.net/jobvious_sprod_db`
